# nixos

Automatic deployment of nixaalb.org nix hosts using [niv](https://github.com/nmattia/niv) and [nixus](https://github.com/Infinisil/nixus).

Secrets are encrypted with [git-crypt](https://www.agwa.name/projects/git-crypt/). To access, ask an already existing gpg collaborator to add your key using `git-crypt add-gpg-user USERID` and push the automatically generated commit to the repo. Pull said commit yourself and then run `git-crypt unlock` to decrypt secrets.

## Updating
run `niv update` in `config/sources/`

## Deploying
run `nix-build deploy` and then `./result` if the derivation builds successfully.
