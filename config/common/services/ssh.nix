{ ... }:
{
	services.openssh = {
		enable = true;
		permitRootLogin = "no";
		passwordAuthentication = false;
		challengeResponseAuthentication = false;
		kexAlgorithms = [ "curve25519-sha256" "curve25519-sha256@libssh.org" ];
	ciphers = [ "chacha20-poly1305@openssh.com" "aes256-gcm@openssh.com" "aes128-gcm@openssh.com" "aes256-ctr" "aes192-ctr" "aes128-ctr" ];
		macs = [ "hmac-sha2-512-etm@openssh.com" "hmac-sha2-256-etm@openssh.com" "umac-128-etm@openssh.com" ];
	extraConfig = "
		HostKeyAlgorithms ssh-ed25519,rsa-sha2-512,rsa-sha2-256
		";
	};

	programs.ssh.knownHosts = {
		despondos = {
			hostNames = [ "despondos.nao.sh" ];
			publicKeyFile = ../data/pubkeys/despondos_host_ed25519.pub;
		};
		vw-backup = {
			hostNames = [ "vw-backup.dotsrc.org" ];
			publicKeyFile = ../data/pubkeys/vw-backup_host_ed25519.pub;
		};
	};

	#services.sshguard = {
	#	enable = true;
	#	blocktime = 300;
	#};
}

