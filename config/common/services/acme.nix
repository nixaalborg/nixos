{ config, ... }:

{
  security.acme = {
    acceptTerms = true;
    email = "admin+certs@nixaalb.org";
  };
}

