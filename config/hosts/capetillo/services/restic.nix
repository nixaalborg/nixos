{ config, ... }:

{

	services.restic.backups = {
		"mail" = {
			paths = [ "/var/vmail" ];
			repository = "sftp:restic@despondos.nao.sh:/etheria/backup/nixaalborg/capetillo/mail";
			initialize = true;
			pruneOpts = [ "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly 75" ];
			timerConfig = { "OnCalendar" = "02:15"; };
			extraOptions = [ "sftp.command='ssh restic@despondos.nao.sh -i ${config.secrets.files.ssh_key.file} -s sftp'" ];
			passwordFile = builtins.toString config.secrets.files.restic_nixaalborg_pass.file;
			user = "virtualMail";
		};
		"vaultwarden" = {
			paths = [ "/var/lib/bitwarden_rs/backup" ];
			repository = "sftp:restic@vw-backup.dotsrc.org:/var/backups/vaultwarden";
			initialize = true;
			pruneOpts = [ "--keep-daily 7" "--keep-weekly 5" "--keep-monthly 12" "--keep-yearly 75" ];
			timerConfig = { "OnCalendar" = "hourly"; };
			extraOptions = [ "sftp.command='ssh restic@vw-backup.dotsrc.org -i ${config.secrets.files.ssh_key.file} -s sftp'" ];
			passwordFile = builtins.toString config.secrets.files.restic_dotsrc_pass.file;
			user = "vaultwarden";
		};
	};
}
