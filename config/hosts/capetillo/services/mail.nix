{ config, ... }:

{
	imports = [
		(builtins.fetchTarball {
			# Pick a commit from the branch you are interested in
			url = "https://gitlab.com/simple-nixos-mailserver/nixos-mailserver/-/archive/6e3a7b2ea6f0d68b82027b988aa25d3423787303/nixos-mailserver-6e3a7b2ea6f0d68b82027b988aa25d3423787303.tar.gz";
			# And set its hash
			sha256 = "1i56llz037x416bw698v8j6arvv622qc0vsycd20lx3yx8n77n44";
		})
	];

	mailserver = {
		enable = true;
		fqdn = "nixaalb.org";
		domains = [ "nixaalb.org" "dotsrc.org"];

		loginAccounts = {
			"emelie@nixaalb.org" = {
				hashedPasswordFile = config.secrets.files.mail_emelie_nixaalb_org.file;
				aliases = [ "admin@nixaalb.org" ];
			};
			"sebastian@nixaalb.org" = {
				hashedPasswordFile = config.secrets.files.mail_sebastian_nixaalb_org.file;
			};
			"noreply@anarkafem.dev" = {
				hashedPasswordFile = config.secrets.files.mail_noreply_anarkafem_dev.file;
			};
			"noreply@dotsrc.org" = {
				hashedPasswordFile = config.secrets.files.mail_noreply_dotsrc_org.file;
			};
		};
		certificateScheme = 3;
	};
}
