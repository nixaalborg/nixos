{config, ...}:
{
services.tor = {
	enable = true;
	enableGeoIP = false;
	settings = {
		HiddenServiceNonAnonymousMode = true;
		Sandbox = true;
	};
	relay.onionServices = {
		nixaalborg = {
			version = 3;
			map = [{
				port = 80;
				target = {
					# TODO: Do this with unix sockets instead
					addr = "[::1]";
					port = 8080;
				};
			}];
			settings = {
				hiddenServiceSingleHopMode = true;
			};
		};
	};
};
}
