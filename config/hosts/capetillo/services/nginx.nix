{ lib, ... }:
{
	imports = [ ../../../common/services/nginx.nix ];
	services.nginx.virtualHosts = {
		"nixaalb.org" = {
			enableACME = true;
			forceSSL = true;
			root = "/var/www/nixaalb.org/public";
			extraConfig = ''
				add_header Content-Security-Policy "script-src 'self'; object-src 'none'; base-uri 'none';" always;
				add_header 'Referrer-Policy' 'same-origin';
				add_header X-Frame-Options DENY;
				add_header X-Content-Type-Options nosniff;
				add_header X-XSS-Protection "1; mode=block";
				add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload";
				add_header Onion-Location http://ag6mlqzpyswq3oogpnuykgllnv5gevjew6dshzmotwgnpo5jw2jqltad.onion$request_uri;
		'';
		};
		"vault.dotsrc.org" = {
			forceSSL = true;
			enableACME = true;
			locations."/" = {
				proxyPass = "http://127.0.0.1:8812";
				proxyWebsockets = true;
			};
			locations."/notifications/hub" = {
				proxyPass = "http://127.0.0.1:3012";
				proxyWebsockets = true;
			};
			locations."/notifications/hub/negotiate" = {
				proxyPass = "http://127.0.0.1:8812";
				proxyWebsockets = true;
			};
		};
		"mta-sts.nixaalb.org" = {
			enableACME = true;
			forceSSL = true;
			root = "/var/www/mta-sts/public";
			extraConfig = ''
				add_header Content-Security-Policy "script-src 'self'; object-src 'none'; base-uri 'none';" always;
				add_header 'Referrer-Policy' 'same-origin';
				add_header X-Frame-Options DENY;
				add_header X-Content-Type-Options nosniff;
				add_header X-XSS-Protection "1; mode=block";
				add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload";
		'';
		};
		"ag6mlqzpyswq3oogpnuykgllnv5gevjew6dshzmotwgnpo5jw2jqltad.onion" = {
			# TODO: Do this with unix sockets instead
			listen = [ {
								addr = "[::1]";
								port = 8080;
							} ];
			root = "/var/www/nixaalb.org/public";
			extraConfig = ''
				add_header Content-Security-Policy "script-src 'self'; object-src 'none'; base-uri 'none';" always;
				add_header 'Referrer-Policy' 'same-origin';
				add_header X-Frame-Options DENY;
				add_header X-Content-Type-Options nosniff;
				add_header X-XSS-Protection "1; mode=block";
				add_header Strict-Transport-Security "max-age=31536000; includeSubdomains; preload";
		'';

		};
	};

	
}
