{ config, ... }:
{
  services.vaultwarden = {
    enable = true;
    environmentFile = config.secrets.files.vaultwarden_env.file;
    backupDir = "/var/lib/bitwarden_rs/backup";
    config = {
      domain = "https://vault.dotsrc.org";
      signupsAllowed = false;
      rocketPort = 8812;
      ipHeader = "X-Real-IP";
      websocketEnabled = true;
      websocketAddress = "127.0.0.1";
      websocketPort = "3012";
      smtpHost = "nixaalb.org";
      smtpFrom = "noreply@dotsrc.org";
      smtpFromName = "Vaultwarden";
      smtpPort = 465;
      smtpSsl = true;
      smtpExplicitTls = true;
      smtpAuthMechanism = "Plain";
    };
  };
	systemd.timers.backup-vaultwarden.timerConfig = { OnCalendar = "hourly"; };
}

