{ config, pkgs, lib, ... }:

{
	imports =
		[ 
		./hardware-configuration.nix
		../../common/services/ssh.nix
		../../common/services/acme.nix
		./services/nginx.nix
		./services/mail.nix
		./services/tor.nix
		./services/vaultwarden.nix
		./services/restic.nix
		./data/secrets/secrets.nix
	];

	boot.loader.grub.enable = true;
	boot.loader.grub.version = 2;
	boot.loader.grub.device = "/dev/sda";
	boot.supportedFilesystems = ["zfs"];
	services.zfs.autoSnapshot.enable = true;
	services.zfs.autoScrub.enable = true;

	networking.hostName = "capetillo"; # Define your hostname.
	networking.hostId = "17a9ec46";
	time.timeZone = "Europe/Copenhagen";
	networking.useDHCP = false;
	networking.interfaces.ens3.useDHCP = true;
	networking.interfaces.ens3.ipv6.addresses = [ { address = "2a01:4f9:c011:50e2::1"; prefixLength = 64; } ];
	networking.defaultGateway6 = { address = "fe80::1"; interface = "ens3"; };

	users.users.emelie = {
		isNormalUser = true;
		extraGroups = [ "wheel" ];
		openssh.authorizedKeys.keys = [
			"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICO4LyBsW1YuUA6i3EL/IZhchSvk7reO4qgRmR/tdQPU emelie@flap"
		];
	};

	users.users.deploy-nix = {
		isNormalUser = true;
	extraGroups = [ "wheel" ];
		openssh.authorizedKeys.keys = [
			"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAICO4LyBsW1YuUA6i3EL/IZhchSvk7reO4qgRmR/tdQPU emelie@flap"
		];
	};

	users.users.deploy-web = {
		isNormalUser = true;
		openssh.authorizedKeys.keys = [ 
			"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILk4m1uJzxd7pDmMZgnZxqD6lEIfVPf+I4tKPo0jJJrK deploy@drone.data.coop"
		];
	};

	users.groups.backup.members = [ "virtualMail" "vaultwarden" ];
	users.groups.nginx.members = [ "deploy-web" ];

	security.sudo.wheelNeedsPassword = false;

	environment.systemPackages = with pkgs; [
		vim
		htop
	iotop
	dig
	];

	nix.trustedUsers = [
		"root"
		"@wheel"
	];

	services.openssh.enable = true;

	networking.firewall.allowedTCPPorts = [ 22 80 193 443 465 587 993 ];
	# networking.firewall.allowedUDPPorts = [ ... ];


	system.stateVersion = "21.05";

}
