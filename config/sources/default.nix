let
  sources = import ./nix/sources.nix;

  # just use standard pkgs from sources
  # so that we have our applyPattches function
  pkgs = import sources.nixpkgs {};

in {
  nixus = sources.nixus;
} // sources
