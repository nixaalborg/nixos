let
	sources = import ../config/sources;
in import "${sources.nixus}" {} ({ config, ... }: {

	defaults = { name, ... }: {
		configuration = { lib, ... }: {
			networking.hostName = lib.mkDefault name;
		};

		# use our nixpkgs from niv
		nixpkgs = sources.nixpkgs;
	};

	nodes = {
		capetillo = { lib, config, ... }: {
			host = "deploy-nix@nixaalb.org";
			configuration = ../config/hosts/capetillo/configuration.nix;
			switchTimeout = 300;
			successTimeout = 300;
			#ignoreFailingSystemdUnits = true;
		};
	};
})
